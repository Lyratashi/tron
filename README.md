# README #

### CHANGELOG ###

* Version 1.3a


```
#!java

FIXED Le score fonctionne maintenant correctement
```

* Version 1.3

```
#!java

ADD Menu
ADD score
```


* Version 1.2


```
#!java

FIXED Class encapsulation
FIXED Class optimisation
ADD On peut jouer à plus de deux joueurs
```


* Version 1.1


```
#!java

ADD Trace Collison
```


* Version 1.0

### SETTINGS ###

* NB_PLAYER = 3
* SPEED = 10.0f
