/**
 *  class Segment
 * 
 * @param depart vecteur de depart
 * @param arrive vecteur de fin
 * 
 * @author Somboom TUNSAJAN & Nicolas HENDRICKX
 */

package be.tunsajan.cm2.labo1;

import java.util.Objects;

public class Segment {

    private final Vecteur2d depart;
    private final Vecteur2d arrive;

    public Segment(Vecteur2d positionDebut, Vecteur2d positionFin) {
        this.depart = positionDebut;
        this.arrive = positionFin;
    }

    private Vecteur2d getDepart() {
        return this.depart;
    }

    private Vecteur2d getArrive() {
        return this.arrive;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.depart);
        hash = 97 * hash + Objects.hashCode(this.arrive);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        
        if (obj == this) {
            return true;
        }
        if (obj instanceof Segment) {
            Segment other = (Segment) obj;
            if (this.getDepart().equals(other.getDepart())) {
                if (this.getArrive().equals(other.getArrive())) {
                    return true;
                }
            }
        }
        return false;

    }

    private static boolean sontEgaux(Segment s1, Segment s2) {
        return s1.equals(s2);
    }

    final public boolean estEnIntersectionAvec(Segment autre) {
        if (Segment.sontEgaux(this, autre)) {
            return true;
        }

        Vecteur2d ceSegmentEnVecteur = enVecteur2d();
        Vecteur2d autreSegmentEnVecteur = autre.enVecteur2d();
        float angleDeLaNormale = Vecteur2d.produitScalaireCroise(ceSegmentEnVecteur, autreSegmentEnVecteur);

        if (angleDeLaNormale == 0) {
            return false;
        }

        Vecteur2d vecteurDepart = Vecteur2d.soustraire(autre.getDepart(), this.getDepart());
        float distance1 = Vecteur2d.produitScalaireCroise(vecteurDepart, autreSegmentEnVecteur) / angleDeLaNormale;
        if (distance1 < 0 || distance1 > 1) {
            return false;
        }

        float distance2 = Vecteur2d.produitScalaireCroise(vecteurDepart, ceSegmentEnVecteur) / angleDeLaNormale;
        return !(distance2 < 0 || distance2 > 1);
    }

    private Vecteur2d enVecteur2d() {
        return new Vecteur2d(this.getArriveX() - this.getDepartX(), this.getArriveY() - this.getDepartY());
    }

    final public static Segment[] enRectangle(Moto m) {
        Segment[] r = 
           {new Segment(new Vecteur2d(m.left(), m.top()), new Vecteur2d(m.right(), m.top())),//Côté supérieur
            new Segment(new Vecteur2d(m.right(), m.top()), new Vecteur2d(m.right(), m.above())),//Côté droit
            new Segment(new Vecteur2d(m.left(), m.above()), new Vecteur2d(m.right(), m.above())),//Côté inférieur
            new Segment(new Vecteur2d(m.left(), m.above()), new Vecteur2d(m.left(), m.top()))};//Côté gauche
        return r;
    }
    private float getDepartX(){
        return this.getDepart().getX();
    }
    private float getDepartY(){
        return this.getDepart().getY();
    }
    private float getArriveX(){
        return this.getArrive().getX();
    }
    private float getArriveY(){
        return this.getArrive().getY();
    }
    final public static float calculerLongueur(Vecteur2d t, Vecteur2d s) {
        return (float) Math.sqrt((float) Math.pow((s.getX() - t.getX()), 2) + (float) Math.pow((s.getY() - t.getY()), 2));
    }

}


