/**
 * 
 * class Moto (herite de la class Rectangle)
 * 
 * Contient toutes les methodes permettant de gerer une moto
 * 
 * @param direction permet de connaitre la direction de la moto
 * @param tm permet de connaitre les différents points de passage de la moto
 * 
 * @author somboom TUNSAJAN (G3)
 * 
 */

package be.tunsajan.cm2.labo1;


final public class Moto extends Rectangle implements Configuration{

    private Vecteur2d direction;
    private final Trace tm;
    
    

    public Moto(float x, float y ) {
        super(x, y, M_HAUTEUR, M_LONGUEUR); /* Appel au constructeur Rectangle */
        this.setDirection(new Vecteur2d(0.0f, -1.0f));
        this.tm = new Trace(new Vecteur2d(x, y));
        this.tournerDeNonanteDegres();
        
    }
    public Moto(Vecteur2d positionBase){
        super(positionBase.getX(), positionBase.getY(), M_HAUTEUR, M_LONGUEUR); /* Appel au constructeur Rectangle */
        this.setDirection(new Vecteur2d(0.0f, -1.0f));
        this.tm = new Trace(new Vecteur2d(positionBase.getX(), positionBase.getY()));
        this.tournerDeNonanteDegres();
        
    }
    
    
    /**
     * 
     * @return le vecteur de direction 
     */
    private Vecteur2d getDirection() {
        return direction;
    }
    
    /**
     * 
     * @return la liste des points de passage (ArrayList) 
     */
    private Trace getTrace(){
        return this.tm;
    }
    /**
     * 
     * @param direction de la moto
     */
    private void setDirection(Vecteur2d direction) {
        this.direction = direction;
    }
    
    /**
     * 
     * Permet faire avancer la moto
     * @param vitesse de la moto
     */
    final public void avancer(float vitesse) {
        this.getDirection().multiplierPar(vitesse);
        this.getVecteur2d().ajouter(this.getDirection());
        this.getDirection().normaliser();
    }

    /**
     *  Fait tourner la moto vers la gauche
     */
    final public void tournerAGauche() {
        this.getDirection().tournerDe(-NONANTE_DEGRES);
        this.tournerDeNonanteDegres();
        this.getTrace().ajouter(this.getVecteur2d());

    }

    /**
     *  Permet de faire tourner la moto vers la droite
     */
    final public void tournerADroite() {
        this.getDirection().tournerDe(NONANTE_DEGRES);
        this.tournerDeNonanteDegres();
        this.getTrace().ajouter(this.getVecteur2d());
    }
    
    /**
     * 
     * @return true si on a une collison avec ca propre trainee 
     */
    final public boolean collisionTrainee() {
        Segment[] r = Segment.enRectangle(this);
        if (this.getTrace().size() > 2) {
            for (int i = 1; i < this.getTrace().size()-1; i++) {
                for (Segment cote :r) {
                    if (cote.estEnIntersectionAvec(new Segment(this.getTrace().retourneElementTrace(i-1), this.getTrace().retourneElementTrace(i)))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * 
     * @param m l autre moto
     * @return vrai en cas de collision avec la trainne d'une autre moto
     */
    final public boolean collisionTrainee(Moto m) {
        Segment[] r = Segment.enRectangle(this);

        if (m.getTrace().size() > 2) {
            for (int i = 1; i < m.getTrace().size(); i++) {
                for (Segment cote : r) {
                    if (cote.estEnIntersectionAvec(new Segment(m.getTrace().retourneElementTrace(i-1), m.getTrace().retourneElementTrace(i)))) {
                        return true;
                    }
                }
            }
        }
        for (Segment cote : r) {
            if (cote.estEnIntersectionAvec(new Segment(m.getTrace().retourneElementTrace(m.getTrace().size() - 1), m.getVecteur2d()))) {
              return true;  
            }
        }
        return false;
        
    }
    
    /**
     * 
     * @return la taille de la liste des points de passage 
     */
    final public int szListePointdePassage(){
        return this.getTrace().size();
    }
    
    /**
     * 
     * @param i indice du point de passage qu on veut obtenir
     * @return le point de passage demande
     */
    final public Vecteur2d pointDePassage(int i){
        return this.getTrace().retourneElementTrace(i);
    }
    final public float distanceFinale(){
        this.getTrace().ajouter(new Vecteur2d(this.getX(), this.getY()));
        return this.getTrace().getDistance();
    }
    final public float distanceParcourue(){
        return this.getTrace().getDistance();
    }

}
