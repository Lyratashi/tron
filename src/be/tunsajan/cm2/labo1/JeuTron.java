/**
 * class jeuTron Ceci est la classe de base du jeu Tron
 *
 * @param player[] un tableau de moto (joueurs)
 * @param murExterne[] un tableau contenant les murs
 *
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo1;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

public class JeuTron implements Configuration {

    private Moto player[];
    private boolean state;
    private Menu menuJeu;
    private float[] score;

    public JeuTron(Menu menuJeu) {

        /* Initialisation des motos */
        this.setPlayer();
        this.stop();
        this.setMenu(menuJeu);
    }

    final public boolean inProgress(){return this.state;}
    final public void stop(){this.state = false;}
    final public void play(){this.state = true;}
    final public float[] getScore(){return score;}
    final public Menu getMenu(){return this.menuJeu;}
    final public void setMenu(Menu m){this.menuJeu = m;}
    /**
     *
     * @return la liste des joueurs
     */
    private Moto[] getPlayer(){return this.player;}

    /**
     * Initialise le tableau de joueurs
     *
     */
    private void setPlayer() {
        this.player = new Moto[NB_PLAYER];
        this.score = new float[NB_PLAYER];
        for (int i = 0; i < NB_PLAYER; i++) {
            this.player[i] = new Moto(TAB_POSITION[i]);
            this.getScore()[i] = 0;
        }
    }

    /**
     * Permet de dessiner le fon de la scene principale (noire)
     *
     * @param cadre gc la scene
     */
    private void dessinerCadre(GraphicsContext cadre) {
        cadre.setFill(Color.BLACK);
        cadre.fillRect(0, 0, W_LONGUEUR, W_HAUTEUR);
    }

    /**
     * Permet de dessiner les differents murs (CYAN)
     *
     * @param murs la scene
     */
    private void dessinerMurs(GraphicsContext murs) {
        murs.setFill(Color.CYAN);
        for (Rectangle wall : MUR)murs.fillRect(wall.left(), wall.top(), wall.getLongueur(), wall.getHauteur());    
    }

    /**
     * Permet de dessiner la moto (WHITE)
     *
     * @param pm la scene
     * @param m la moto
     */
    private void dessinerMoto(GraphicsContext pm, Moto m) {
        pm.setFill(Color.WHITE);
        pm.fillRect(m.left(), m.top(), m.getLongueur(), m.getHauteur());

    }
        public static void switchColorStroke(int num, GraphicsContext gc) {
        switch (num) {
            case 0:
                gc.setStroke(Color.RED);
                break;
            case 1:
                gc.setStroke(Color.GREEN);
                break;
            case 2:
                gc.setStroke(Color.BLUE);
                break;
        }
    }

    /**
     * Permet de dessiner une trace d une moto
     *
     * @param tr la scene
     * @param m la moto
     * @param numMoto le numero de la moto
     */
    private void dessinerTrace(GraphicsContext tr, Moto m, int numMoto) {
        this.switchColorStroke(numMoto, tr); /* on prends une couleur */

        tr.beginPath();
        /* on recupere les different pointDePassge affin de les dessiner */
        for (int i = 0; i < m.szListePointdePassage(); i++) {
            Vecteur2d v = m.pointDePassage(i);
            tr.lineTo(v.getX(), v.getY()); /* on genere les differents points */
        }
        tr.lineTo(m.getX(), m.getY());
        tr.stroke(); /* On dessine la trace */
        tr.closePath();
    }

    /**
     * Permet de dessiner les differents objets (sera mis a jour 30x par sec)
     *
     * @param peintre la scene
     */
    final public void dessinerScene(GraphicsContext peintre) {
        if (this.inProgress())mettreAJour(); /* Mise a jour des differents elements */
        this.dessinerCadre(peintre);
        this.dessinerMurs(peintre);

        /* On dessinne moto par moto */
        for (int j = 0; j < this.getPlayer().length; j++) {
            this.dessinerMoto(peintre, this.getPlayer()[j]);
            this.dessinerTrace(peintre, this.getPlayer()[j], j);
        }
        this.dessinerScore(peintre);
    }
    private void dessinerScore(GraphicsContext gc) {

        for (int i = 0; i < NB_PLAYER; i++) {
            this.switchColorStroke(i, gc);
            gc.strokeText("PLAYER " + (i + 1) + ": \n", 20, (60 + (i * 18)));
            gc.setStroke(Color.WHITE);
            gc.strokeText("" + this.getPlayer()[i].distanceParcourue(), 115, (60 + (i * 18)));

        }
    }

    /**
     * permet de gerer les differents collisions entre les murs
     *
     * @param numMoto numero de la moto
     * @return true si collision
     */
    private boolean gestionCollisionMur(int numMoto) {
        /* On test les differents murs */
        for (Rectangle murs : MUR) if (this.getPlayer()[numMoto].collision(murs)) return true;
        return false;
    }

    /**
     * Permet de gerer la colision entre deux motos
     *
     * @param numMoto moto de base
     * @param j moto de colision
     * @return true en cas de colision
     */
    private boolean gestionCollisionMoto(int numMoto, int j){return (this.getPlayer()[numMoto].collision(this.getPlayer()[j]));}
    /**
     * Permet de detecter une colision avec une trace d une AUTRE moto
     *
     * @param numMoto moto de base
     * @param j l autre moto
     * @return true en cas de colison
     */
    private boolean gestionCollisionTraine(int numMoto, int j){return (this.getPlayer()[numMoto].collisionTrainee(this.getPlayer()[j]));}
    /**
     * Permet de detecter la colision entre une moto et sa PROPRE trainee
     *
     * @param numMoto moto de bas
     * @return true en cas de colision
     */
    private boolean gestionCollisionTraine(int numMoto){return (this.getPlayer()[numMoto].collisionTrainee());}
    /**
     * Permet de mettre a jour les objets qui interagissent enssemble sur la
     * scene
     */
    private void mettreAJour() {

        /* on effectue le traitement sur chaque moto */
        for (int numMoto = 0; numMoto < this.getPlayer().length; numMoto++) {
            this.getPlayer()[numMoto].avancer(SPEED);
            if (this.collision(numMoto)) this.finDePartie();
        }
    }

    private boolean collision(int numMoto) {
        /* colision avec un des murs */
        if (this.gestionCollisionMur(numMoto))return true;
        for (int j = 0; j < this.getPlayer().length; j++) { /* Collision avec une moto */

            /* Detection des colisoon entre une autre moto ou une autre traine */
            if (numMoto != j) {
                if (this.gestionCollisionMoto(numMoto, j)) return true;
                if (this.gestionCollisionTraine(numMoto, j))return true;
            }
            /* Detection de la colision aec sa propre trainee */
            if (this.gestionCollisionTraine(numMoto)) return true;
        }
        return false;
    }

    final public void gererTouche(KeyEvent evt) {
        if (!this.inProgress()) {
            if (" ".equals(evt.getCharacter()))this.play();
            else return;
        }
        switch (NB_PLAYER) {
            case 1:
                this.gererToucheUnJoueur(evt);
                break;
            case 2:
                this.gererToucheDeuxJoueurs(evt);
                break;
            case 3:
                this.gererToucheTroisJoueurs(evt);
                break;

        }
    }

    /**
     *
     * Permet de manipuler les moto Joueur 1: a z
     *
     * (respectivement a gauche et a droite
     *
     * @param evt recuperation des touches
     */
    private void gererToucheUnJoueur(KeyEvent evt) {
        switch (evt.getCharacter()) {
            case "a":
                this.getPlayer()[0].tournerAGauche();
                break;
            case "z":
                this.getPlayer()[0].tournerADroite();
                break;
        }
    }

    /**
     *
     * Permet de manipuler les moto Joueur 1: a z joueur 2: o p (respectivement
     * a gauche et a droite
     *
     * @param evt recuperation des touches
     */
    private void gererToucheDeuxJoueurs(KeyEvent evt) {
        switch (evt.getCharacter()) {
            case "a":
                this.getPlayer()[0].tournerAGauche();
                break;
            case "z":
                this.getPlayer()[0].tournerADroite();
                break;
            case "o":
                this.getPlayer()[1].tournerAGauche();
                break;
            case "p":
                this.getPlayer()[1].tournerADroite();
                break;
        }
    }

    /**
     *
     * Permet de manipuler les moto Joueur 1: a z joueur 2: o p joueur 3: b n
     * (respectivement a gauche et a droite
     *
     * @param evt recuperation des touches
     */
    private void gererToucheTroisJoueurs(KeyEvent evt) {
        switch (evt.getCharacter()) {
            case "a":
                this.getPlayer()[0].tournerAGauche();
                break;
            case "z":
                this.getPlayer()[0].tournerADroite();
                break;
            case "o":
                this.getPlayer()[1].tournerAGauche();
                break;
            case "p":
                this.getPlayer()[1].tournerADroite();
                break;
            case "b":
                this.getPlayer()[2].tournerAGauche();
                break;
            case "n":
                this.getPlayer()[2].tournerADroite();
                break;
        }
    }

    private void finDePartie() {
        this.stop();
        for (int i = 0; i < NB_PLAYER; i++)this.getScore()[i] = this.getPlayer()[i].distanceFinale();
        this.getMenu().setScoreFinal(this.getScore());
        this.getMenu().gameOver();
    }
}
