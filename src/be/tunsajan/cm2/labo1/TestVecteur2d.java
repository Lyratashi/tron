package be.tunsajan.cm2.labo1;

/**
 * Cette classe regroupe des tests validant la classe Vecteur2d à développer.
 * @author Nicolas Hendrikx
 */
public class TestVecteur2d {
    public static void main(String[] args) {
        System.err.println("Début des tests");
        testerLongueur();
        testerNormalisation();
        testerMultiplication();
        testerAjout();
        testerTournerDe();
        System.err.println("Fin des tests");
    }
    
    public static void testerLongueur() {
       System.err.println("===Tests de la méthode longueur===");
       Vecteur2d vecteurNul = new Vecteur2d();
       Vecteur2d vecteurUnitaireX = new Vecteur2d(1,0);
       Vecteur2d vecteurUnitaireY = new Vecteur2d(0,1);
       Vecteur2d vecteurUnUn = new Vecteur2d(1,1);
        Vecteur2d vecteurMoinsTroisQuatre = new Vecteur2d(-3,4);
        
      assert vecteurNul.longueur() == 0.0f;
        assert vecteurUnitaireX.longueur() == 1.0f;
        assert vecteurUnitaireY.longueur() == 1.0f;
       //Les longueurs suivantes peuvent être impactées par des erreurs d'arrondi.
       //Pour en tenir compte, les tests mesurent l'écart absolue entre la longueur produite
        //et celle attendue. Le test passe lorsque l'écart est suffisament faible.
        assert Math.abs(vecteurUnUn.longueur()-1.41421356) < 0.0001;
        assert Math.abs(vecteurMoinsTroisQuatre.longueur() - 5.0f) < 0.0001;
        
        System.err.println("===Fin des tests de la méthode longueur===");
    }
    
   public static void testerNormalisation() {
        System.err.println("===Début des tests de la méthode Vecteur2d::normaliser===");
        Vecteur2d vecteurUnitaireX = new Vecteur2d(1,0);
        Vecteur2d vecteurUnitaireY = new Vecteur2d(0,1);
        Vecteur2d vecteurUnUn = new Vecteur2d(1,1);
        Vecteur2d vecteurMoinsTroisQuatre = new Vecteur2d(-3,4);
       Vecteur2d vecteurNul = new Vecteur2d();
        
        vecteurUnitaireX.normaliser();
        vecteurUnitaireY.normaliser();
       vecteurUnUn.normaliser();
        vecteurMoinsTroisQuatre.normaliser();
        vecteurNul.normaliser();
       
       assert vecteurUnitaireX.longueur() == 1.0f;
        assert vecteurUnitaireY.longueur() == 1.0f;
      assert Math.abs(vecteurUnUn.longueur() - 1.0f) < 0.0001;
        assert Math.abs(vecteurMoinsTroisQuatre.longueur() - 1.0f) < 0.0001;
        assert vecteurNul.longueur() == 0.0f;
      
        System.err.println("===Fin des tests de la méthode Vecteur2d::normaliser===");
    }

    public static void testerMultiplication() {
       System.err.println("===Début des tests de la méthode Vecteur2d::multiplierPar===");
       Vecteur2d vecteurUnitaireX = new Vecteur2d(1,0);
     Vecteur2d vecteurUnitaireY = new Vecteur2d(0,1);
       Vecteur2d vecteurUnUn = new Vecteur2d(1,1);
       Vecteur2d vecteurMoinsTroisQuatre = new Vecteur2d(-3,4);
        Vecteur2d vecteurNul = new Vecteur2d();
       
        vecteurUnitaireX.multiplierPar(0);
      assert vecteurUnitaireX.getX() == 0 && vecteurUnitaireX.getY() == 0;
       
       vecteurUnitaireY.multiplierPar(10);
      assert vecteurUnitaireY.getX() == 0 && vecteurUnitaireY.getY() == 10;
       
     vecteurMoinsTroisQuatre.multiplierPar(-1.0f);
       assert vecteurMoinsTroisQuatre.getX() == 3 && vecteurMoinsTroisQuatre.getY() == -4;
     
      vecteurNul.multiplierPar(10);
       assert vecteurNul.getX() == 0.0f && vecteurNul.getY() == 0.0f;
       
       System.err.println("===Fin des tests de la méthode Vecteur2d::multiplierPar===");
    }

   public static void testerAjout() {
          Vecteur2d vecteurUnitaireX = new Vecteur2d(1,0);
       Vecteur2d vecteurUnitaireY = new Vecteur2d(0,1);
       Vecteur2d vecteurUnUn = new Vecteur2d(1,1);
       Vecteur2d vecteurMoinsTroisQuatre = new Vecteur2d(-3,4);
      Vecteur2d vecteurNul = new Vecteur2d();
       
       vecteurUnitaireX.ajouter(new Vecteur2d(0, 1));
       assert vecteurUnitaireX.getX() == 1 && vecteurUnitaireX.getY() == 1;
       
       vecteurUnitaireY.ajouter(new Vecteur2d(0,-1));
       assert vecteurUnitaireY.getX() == 0 && vecteurUnitaireY.getY() == 0;
       
       vecteurUnUn.ajouter(new Vecteur2d(5,10));
        assert vecteurUnUn.getX() == 6 && vecteurUnUn.getY() == 11;
       
      vecteurMoinsTroisQuatre.ajouter(new Vecteur2d());
        assert vecteurMoinsTroisQuatre.getX() == -3 && vecteurMoinsTroisQuatre.getY() == 4;        
       vecteurNul.ajouter(null);
       assert vecteurNul.getX() == 0 && vecteurNul.getY() == 0;
        
       System.err.println("===Fin des tests de la méthode Vecteur2d::ajouter===");
    }
    
    public static void testerTournerDe() {
       final float NONANTE_DEGRES = (float)Math.PI/2.0f;
       
      System.err.println("===Début des tests de la méthode Vecteur2d::ajouter===");
      Vecteur2d vecteurUnitaireX = new Vecteur2d(1,0);        
      Vecteur2d vecteurUnitaireY = new Vecteur2d(0,1);
       Vecteur2d vecteurUnUn = new Vecteur2d(1,1);
       Vecteur2d vecteurNul = new Vecteur2d();
       
        vecteurUnitaireX.tournerDe(NONANTE_DEGRES);       
        verifierVecteur2dVaut(vecteurUnitaireX, 0.0f, 1.0f);
       
       vecteurUnitaireY.tournerDe(-NONANTE_DEGRES);
      verifierVecteur2dVaut(vecteurUnitaireY, 1.0f, 0.0f);
        vecteurUnitaireY.tournerDe(NONANTE_DEGRES);
        verifierVecteur2dVaut(vecteurUnitaireY, 0.0f, 1.0f);
       
        vecteurUnUn.tournerDe(2*NONANTE_DEGRES);
       verifierVecteur2dVaut(vecteurUnUn, -1.0f, -1.0f);
        vecteurUnUn.tournerDe(NONANTE_DEGRES);
        verifierVecteur2dVaut(vecteurUnUn, 1.0f, -1.0f);
      
        vecteurNul.tournerDe(NONANTE_DEGRES);
        verifierVecteur2dVaut(vecteurNul, 0, 0);
      
       System.err.println("===Fin des tests de la méthode Vecteur2d::tournerDe===");
    }
    
   public static void verifierVecteur2dVaut(Vecteur2d v, float xAttendu, float yAttendu) {
       assert Math.abs(v.getX() - xAttendu) < 0.00001f : String.format("valeur de x reçu : %f. Valeur attendue : %f", v.getX(), xAttendu);
        assert Math.abs(v.getY() - yAttendu) < 0.00001f : String.format("valeur de y reçu : %f. Valeur attendue : %f", v.getY(), yAttendu);
   }
}
