/**
 *  CM_2: Labo_1 Tron
 *  jeu Tron il s'agit de faire interagir des motos
 *  elles ne peuvent pas se toucher ni toucher les murs
 *  elles ne peuvent pas toucher leur propre trace ni la
 *  trace de l autre joueur
 *  J_1: a z
 *  J_2: o p
 *  J_3: b n
 * (On peut eventuellement modiffier le nombre de joueurs)
 * @author TUNSAJAN somboom (G3) (Nicolas HENDRICKX m' a aidé xD)
 */
package be.tunsajan.cm2.labo1;

import nhpack.SceneBuilderFactory;

public class Programme implements Configuration {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        if(NB_PLAYER>3 || NB_PLAYER<=0){
            System.err.println("config.NB_PLAYER ...fail ");
            System.exit(NB_PLAYER);
        }
        Menu jeu = new Menu();
        
        
        SceneBuilderFactory.newInstance()
                .withDimension(W_LONGUEUR, W_HAUTEUR)
                .withTitle("Tron (multijoueurs) - Le jeu")
                .withDrawMethod(jeu::dessinerScene)
                .withKeyTypeHandler(jeu::gererTouche)
                .finish();
    }
    
}
