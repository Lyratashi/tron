/**
 *  class Vecteur2d
 *  Permet de gerer un Vecteur à deux dimensions
 *  @param x sa coordonnee en x
 *  @param y sa coordonnee en y
 * 
 *  @author Somboom TUNSAJAN (G3)
 */

package be.tunsajan.cm2.labo1;

public class Vecteur2d {

    private float x;
    private float y;

    public Vecteur2d() {
        this.setX(0.00f);
        this.setY(0.00f);

    }

    public Vecteur2d(float x, float y) {
        this.setX(x);
        this.setY(y);
    }
    public Vecteur2d(Vecteur2d v){
        this.setX(v.getX());
        this.setY(v.getY());
    }
    
    /**
     * 
     * @return le vecteur lui meme 
     */
    final protected Vecteur2d getVecteur2d(){
        return this;
    }
    
    /**
     * 
     * @return sa coordonnee en x
     */
    final protected float getX() {
        return x;
    }

    final protected void setX(float x) {
        this.x = x;
    }
    
    /**
     * 
     * @return sa cooedonnees en y 
     */
    final protected float getY() {
        return y;
    }
    
    /**
     * 
     * @param y sa coordonnee en y 
     */
    final protected void setY(float y) {
        this.y = y;
    }

    /**
     * 
     * @return la longueur du vecteur 
     */
    final protected float longueur() {
        return (float) Math.sqrt(Math.pow(this.getX(), 2) + Math.pow(this.getY(), 2));

    }
    
    /**
     * Normalise le vecteur
     */
    final protected void normaliser() {
        float tmp = this.longueur();
        if (tmp > 0.0f) {
            this.setX(this.getX() / tmp);
            this.setY(this.getY() / tmp);
        }

    }
    
    /**
     * 
     * Multiple le vecteur par un scalaire
     * @param scalaire par le quel on veut multiplier le vecteur
     */
    final protected void multiplierPar(float scalaire) {
        this.setX(this.getX() * scalaire);
        this.setY(this.getY() * scalaire);

    }
    
    /**
     * Additionne deux vecteurs
     * @param v le vecteur a aditionner
     */
    final protected void ajouter(Vecteur2d v) {
        if (v == null) {
            return;
        }
        this.setX(this.getX() + v.getX());
        this.setY(this.getY() + v.getY());
    }
    
    /**
     * Permet de soustraire deux vecteurs
     * @param v1 
     * @param v2
     * @return un vecteur resultant de la soustraction des deux vecteurs
     */
    final public static Vecteur2d soustraire(Vecteur2d v1, Vecteur2d v2){
        return new Vecteur2d(v1.getX() - v2.getX(),  v1.getY() - v2.getY());
    }
    
    /**
     * Permet de faire faire une rotation au vecteur
     * @param radian angle de rotation exprime en radian
     */
    final protected void tournerDe(float radian) {
        if (radian == 0) {
            return;
        }
        if (this.longueur() == 0) {
            return;
        }
        float xtmp = this.getX();
        float ytmp = this.getY();
        this.setX(((float) Math.cos(radian) * xtmp) - ((float) Math.sin(radian) * ytmp));
        this.setY(((float) Math.sin(radian) * xtmp) + ((float) Math.cos(radian) * ytmp));
       
    }
    
    /**
     * 
     * @param v1
     * @param v2
     * @return le resultat du produit scalaire croise 
     */
    final public static float produitScalaireCroise(Vecteur2d v1 , Vecteur2d v2){
        return (v1.getX() * v2.getY()) - (v1.getY() * v2.getX());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Float.floatToIntBits(this.x);
        hash = 43 * hash + Float.floatToIntBits(this.y);
        return hash;
    }
   
    
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj instanceof Vecteur2d) {
            Vecteur2d other = (Vecteur2d) obj;
            if( this.getX() == other.getX()){
                if(this.getY() == other.getY()) return true;
            }
        }
        return false;
    }

}
