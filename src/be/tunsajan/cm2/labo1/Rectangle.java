/**
 * class Rectangle
 *  Tout ce qu il faut comme information sur un rectangle
 *  Herite de vecteur2d et de certaine methode de celle-ci
 * 
 *  @param hauteur du rectangle (largeur ?)
 *  @param lougueur du rectangle
 * 
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo1;

public class Rectangle extends Vecteur2d implements Configuration {
    private float hauteur;
    private float longueur;
    /* NOTE: un heritage d une classe centre aurait ete lieux mais c est pour respecter l ennonce */
    
    public Rectangle(float x, float y, float hauteur, float longueur){
        
        super(x, y); /* Appel au constructeur Vecteur2d */
        this.setHauteur(hauteur);
        this.setLongueur(longueur);
    }
    /**
     * 
     * @return la hauteur du rectangle 
     */
    final protected float getHauteur() {
        return hauteur;
    }
    
    /**
     * 
     * @param hauteur du rectangle
     */
    final protected void setHauteur(float hauteur) {
        this.hauteur = hauteur;
    }
    
    /**
     * 
     * @return la longueur du rectangle 
     */
    final protected float getLongueur() {
        return longueur;
    }
    
    /**
     * 
     * @param longueur du rectangle 
     */
    final protected void setLongueur(float longueur) {
        this.longueur = longueur;
    }
    
    /**
     * 
     * @return la coordonee y du haut du rectangle 
     */
    final protected float top(){
        /* /2 car par rapport au centre */
        return this.getY() - this.getHauteur()/2; 
    }
    
    /**
     * 
     * @return la coordonnee y du dessous du rectangle 
     */
    final protected float above(){
        /* /2 car par rapport au centre */
        return this.getY() + this.getHauteur()/2;
    }
    
    /**
     * 
     * @return la coordonnee x du cote gauche du rectangle
     */
    final protected float left(){
        /* /2 car par rapport au centre */
        return this.getX() - this.getLongueur()/2;
    }
    
    /**
     * 
     * @return la coordonnee y du cote du rectangle 
     */
    final protected float right(){
        /* /2 car par rapport au centre */
        return this.getX() + this.getLongueur()/2; 
    }
    
    /**
     * Permet de faire tourne le rectangle de 90 degres sur lui meme
     */
    final protected void tournerDeNonanteDegres() {
        float ancienneHauteur = this.getHauteur();
        this.setHauteur(this.getLongueur());
        this.setLongueur(ancienneHauteur);
    } 
    
    /**
     * 
     * @param r le rectangle qui rentre en collision
     * @return true en cas de collision
     */
    final protected boolean collision(Rectangle r){
        return (Math.abs(this.getX()- r.getX()) <= (this.getLongueur() / 2 + r.getLongueur() / 2)
                && Math.abs(this.getY() - r.getY()) <= this.getHauteur() / 2 + r.getHauteur() / 2);
        
    }
}