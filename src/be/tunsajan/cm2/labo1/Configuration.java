/**
 * 
 * interface contenant les differentes constantes du programme Tron
 * 
 * @author Tunsajan Somboom (g3)
 */
package be.tunsajan.cm2.labo1;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public interface Configuration {
    final int NB_PLAYER = 2;
    final float NONANTE_DEGRES = (float) Math.PI / 2.0f; /* ANGLE EN RADIAN */
    final int W_LONGUEUR = 1024;
    final int W_HAUTEUR = 728;
    final int M_HAUTEUR = 10;
    final int M_LONGUEUR = 40;
    final float SPEED = 10.0f;
    final Vecteur2d TAB_POSITION[] = {new Vecteur2d(W_LONGUEUR/ 2,  W_HAUTEUR /2),
                                               new Vecteur2d(W_LONGUEUR / 3, W_HAUTEUR /2),
                                               new Vecteur2d((W_LONGUEUR / 2) + (W_LONGUEUR/5), W_HAUTEUR /2)};

    final Rectangle MUR[] =  {   new Rectangle(0, W_HAUTEUR / 2, W_LONGUEUR, 10),
                                  new Rectangle(W_LONGUEUR, W_HAUTEUR / 2, W_HAUTEUR, 10),
                                  new Rectangle(W_LONGUEUR / 2, 0, 10, W_LONGUEUR),
                                  new Rectangle(W_LONGUEUR / 2, W_HAUTEUR, 10, W_LONGUEUR)};
}
