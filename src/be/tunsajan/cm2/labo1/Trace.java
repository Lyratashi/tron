/**
 *
 * class Trace
 * 
 * Une liste de Vecteur2D permettant de gerer les points de passages 
 * d' une moto
 * 
 * @param ArrayList<Vecteur2d> une liste de Vecteur2d
 * @param distante la longueur de la trace
 * 
 * @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo1;

import java.util.ArrayList;


final public class Trace {

    private ArrayList<Vecteur2d> traceMoto;
    private float distance;

    public Trace(Vecteur2d v) {
        this.setTrace(new ArrayList<>());
        this.getListTrace().add(new Vecteur2d(v));
        this.setDistance(0);

    }
    private void setTrace(ArrayList<Vecteur2d> list){
        this.traceMoto = list;
        
    }
    private void setDistance(float dist){
        this.distance+=dist;
    }
    final public float getDistance(){
        
        return this.distance;
    }
    /**
     * 
     * @return la liste des points de passages 
     */
    private ArrayList<Vecteur2d>  getListTrace() {
        return this.traceMoto;
    }
    
    /**
     * Permet d ajouter un element de type Vecteur2d
     * 
     * @param v Vecteur2d a ajouter 
     */
    final void ajouter(Vecteur2d v) {
        
        this.setDistance(Segment.calculerLongueur(this.getListTrace().get(this.size()-1), v));
        this.getListTrace().add(new Vecteur2d(v));

    }
    /**
     * 
     * @param i indice de l element a retourner
     * @return l element à l indice voulu
     */
    final public Vecteur2d retourneElementTrace(int i){
        return this.getListTrace().get(i);
    }
    final public int size(){
        return this.getListTrace().size();
    }
    

}

