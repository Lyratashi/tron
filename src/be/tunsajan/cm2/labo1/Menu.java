/**
 *  class Menu
 *  Menu du jeuTron
 * 
 *  @param font permet de gerer la taille des caracteres
 *  @param game une reference vers jeuTron
 * 
 *  @author Somboom TUNSAJAN (G3)
 */
package be.tunsajan.cm2.labo1;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


final public class Menu implements Configuration {

    private Font font; // Gere la taille de caractere
    private JeuTron game;
    private float[] scoreFinal;

    public Menu() {
        this.setGame(null);
        this.setFont(new Font(20));
        this.setScoreFinal();

    }

    private void setGame(JeuTron g) {
        this.game = g;
    }
    private void setScoreFinal(){
        this.scoreFinal = new float[NB_PLAYER];
        for (int i = 0; i < NB_PLAYER; i++) {
            this.scoreFinal[i] = -1;
        }
    }
    final public void setScoreFinal(float[] s) {
        System.arraycopy(s, 0, this.scoreFinal, 0, NB_PLAYER);
    }
    final public float[] getScoreFinal(){
        return this.scoreFinal;
    }

    /**
     *
     * @return
     */
    private Font getFont() {
        return this.font;
    }

    /**
     *
     * @param font
     */
    private void setFont(Font font) {
        this.font = font;
    }

    private JeuTron getGame() {
        return this.game;
    }

    private void nouvellePartie() {
        this.setGame(new JeuTron(this));
    }

    final public void gameOver() {
        this.setGame(null);
    }

    /**
     *
     * @param evt
     */
    final public void gererTouche(javafx.scene.input.KeyEvent evt) {
        if (this.getGame() != null) {
            this.getGame().gererTouche(evt);
        } else {
            switch (evt.getCharacter()) {
                case "n":
                case "N":
                    this.nouvellePartie(); //On demarre une nouvelle partie
                    break;
                case "q":
                case "Q":
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }

    }

    /**
     * JeuPong prends la main
     *
     * @param peintre
     */
    private void playGame(GraphicsContext peintre) {
        this.setFont(new Font(18));
        peintre.setFont(this.getFont());
        this.getGame().dessinerScene(peintre);
    }

    /**
     * Menu prends la main
     *
     * @param peintre
     */
    private void playMenu(GraphicsContext peintre) {
        this.setFont(new Font(20));
        peintre.setFont(this.getFont());
        peintre.setFill(Color.BLACK);
        peintre.fillRect(0, 0, W_LONGUEUR, W_HAUTEUR);
        peintre.setStroke(Color.CYAN);
        peintre.strokeText("____T__R__O__N____", (W_LONGUEUR / 2) - 100, (W_HAUTEUR / 2) - 60);
        peintre.strokeText("     Jouer [N]", (W_LONGUEUR / 2) - 85, (W_HAUTEUR / 2));
        peintre.strokeText("   Quitter [Q]", (W_LONGUEUR / 2) - 87, (W_HAUTEUR / 2) + 40);
    }

    private void dessinerScore(GraphicsContext gc) {

        for (int i = 0; i < NB_PLAYER; i++) {
            JeuTron.switchColorStroke(i, gc);
            gc.strokeText("PLAYER " + (i + 1) + ": \n", 20, (60 + (i * 18)));
            gc.setStroke(Color.WHITE);
            gc.strokeText("" + this.getScoreFinal()[i], 115, (60 + (i * 18)));

        }
    }

    /**
     *
     * @param peintre
     */
    final public void dessinerScene(GraphicsContext peintre) {
        if (this.getGame() != null) {
            this.playGame(peintre);
            return;
        }
        this.playMenu(peintre);
        if (this.getScoreFinal()[0] > -1) {
            this.dessinerScore(peintre);
        }
    }

}
